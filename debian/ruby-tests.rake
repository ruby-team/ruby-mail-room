require 'gem2deb/rake/spectask'
task :default do
  Rake::Task[:start_redis].invoke
  failed = false
  begin
    Rake::Task[:test].invoke
  rescue
    failed = true
  end
end

task :start_redis do
    sh 'redis-server --daemonize yes --port 6379&'
end

Gem2Deb::Rake::RSpecTask.new do |spec|
    spec.pattern = './spec/**/*_spec.rb'
end

task :stop_redis do
    # In autopkgtest redis service is run as redis user by default. So debci cannot kill that process.
    sh 'pkill redis-server || true'
end

at_exit { Rake::Task[:stop_redis].invoke }
