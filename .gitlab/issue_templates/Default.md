Please read me!

This repository supports our packaged release of `mail_room` that ships with GitLab. We strive to diverge as little as possible from the canonical upstream at https://www.github.com/tpitale/mail_room. If we do diverge, our patches are sent upstream for review and we carry them here to support the GitLab application.

You can raise an issue here if you're having an issue *specifically in the context of a GitLab installation*.

Otherwise, please direct any issues upstream to https://www.github.com/tpitale/mail_room.

Thank you for contributing!